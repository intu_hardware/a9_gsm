#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "api_os.h"
#include "api_debug.h"
#include "api_event.h"
#include "api_network.h"
#include "api_socket.h"
#include "api_lbs.h"
#include <api_hal_uart.h>
#include <string.h>
#include <stdio.h>
#include <api_os.h>
#include <api_gps.h>
#include <api_event.h>
#include <api_hal_uart.h>
#include <api_debug.h>
#include "math.h"
#include "api_hal_pm.h"
#include "time.h"
#include "api_info.h"
#include "assert.h"
#include "api_socket.h"
#include "api_network.h"
#include "api_hal_gpio.h"
#include "api_sms.h"

#define TEST_PHONE_NUMBER "+919606574808"
uint8_t utf8Msg[400]    = "Reset Successfull";
uint8_t unicodeMsg[] = {0x1F,0x44,0x0E};


#define SERVER_IP   "13.127.218.124"
#define SERVER_PORT  2222

#define SYSTEM_STATUS_LED GPIO_PIN27
#define UPLOAD_DATA_LED   GPIO_PIN28



#define MAIN_TASK_STACK_SIZE    (2048 * 2)
#define MAIN_TASK_PRIORITY      0
#define MAIN_TASK_NAME          "Main Test Task"

#define SECOND_TASK_STACK_SIZE    (2048 * 2)
#define SECOND_TASK_PRIORITY      1
#define SECOND_TASK_NAME          "Second Test Task"

static HANDLE mainTaskHandle = NULL;
static HANDLE secondTaskHandle = NULL;
static bool flag = false, flag2 = true;

char info[50000];
char final[50000];
char t[400];
char hexaDeciNum[100];
char hexaDeciNumR[100];


char buffer[400],batt[100],imei[100];

void EventDispatch(API_Event_t* pEvent)
{
    switch(pEvent->id)
    {
        case API_EVENT_ID_NO_SIMCARD:
            //Trace(10,"!!NO SIM CARD%d!!!!",pEvent->param1);
            break;

        case API_EVENT_ID_SYSTEM_READY:
            //Trace(1,"system initialize complete");
            break;

        case API_EVENT_ID_NETWORK_REGISTERED_HOME:
        case API_EVENT_ID_NETWORK_REGISTERED_ROAMING:
            //Trace(2,"network register success");
            Network_StartAttach();
            break;

        case API_EVENT_ID_NETWORK_ATTACHED:
            Trace(2,"network attach success");
            Network_PDP_Context_t context = {
                .apn        ="cmnet",
                .userName   = ""    ,
                .userPasswd = ""
            };
            Network_StartActive(context);
            break;

        case API_EVENT_ID_NETWORK_ACTIVATED:
            //Trace(2,"network activate success");
            flag = true;
            break;

        case API_EVENT_ID_NETWORK_CELL_INFO:
        {
            uint8_t number = pEvent->param1;
            Network_Location_t* location = pEvent->pParam1;
            //Trace(2,"network cell infomation,serving cell number:1, neighbor cell number:%d",number-1);

            //char num[100];
            //sprintf(num,"%d",number);
            
            for(int i=0;i<number;++i)
            {
                Trace(2,"cell %d info:%d%d%d,%d%d%d,%d,%d,%d,%d,%d,%d",i,
                location[i].sMcc[0], location[i].sMcc[1], location[i].sMcc[2], 
                location[i].sMnc[0], location[i].sMnc[1], location[i].sMnc[2],
                location[i].sLac, location[i].sCellID, location[i].iBsic,
                location[i].iRxLev, location[i].iRxLevSub, location[i].nArfcn);

                char mcc1[20],mcc2[20],mcc3[20],mnc1[20],mnc2[20],lac[20],cellid[20],bsic[20],rxlvl[20],arfcn[20];


                

                

                //strcat(info,"MCC:");
                sprintf(mcc1,"%d",location[i].sMcc[0]);
                strcat(info,mcc1);
                sprintf(mcc2,"%d",location[i].sMcc[1]);
                strcat(info,mcc2);
                sprintf(mcc3,"%d",location[i].sMcc[2]);
                strcat(info,mcc3);
                char s[10]=","; strcat(info,s);
                sprintf(mnc1,"%d",location[i].sMnc[0]);
                strcat(info,mnc1);
                sprintf(mnc2,"%d",location[i].sMnc[1]);
                strcat(info,mnc2); strcpy(s,","); strcat(info,s);
                //sprintf(mnc3,"%d",location[i].sMnc[2]);
                sprintf(rxlvl,"%d",location[i].iRxLev*-1);
                strcat(info,rxlvl);strcpy(s,","); strcat(info,s);
                
                decToHexa(location[i].sCellID);
                
                strcat(info,hexaDeciNumR);strcpy(s,","); strcat(info,s);


                sprintf(arfcn,"%d",location[i].nArfcn);
                strcat(info,arfcn);strcpy(s,",");strcat(info,s);
                
                decToHexa(location[i].sLac);
                //sprintf(lac,"%d",location[i].sLac);
                strcat(info,hexaDeciNumR);
                strcat(info,"|");

            }
            
            strcpy(final,info);
            strcpy(info,"\0");
            
            //UART_Write(UART1,number,strlen(number));


            
            //strcat(t,final);
            //UART_Write(UART1,t,strlen(t));
            UART_Write(UART1,final,strlen(final));
            
            //UART_Write(UART1,"\t",strlen("here\t"));

            float longitude,latitude;
            if(!LBS_GetLocation(location,number,15,&longitude,&latitude))
            {
                //Trace(1,"===LBS get location fail===");
            }
            else
            {
                //Trace(1,"===LBS get location success,latitude:%d.%d,longitude:%d.%d===",(int)latitude, (int)((latitude-(int)latitude)*100000),
                                                                               //  (int)longitude, (int)((longitude-(int)longitude)*100000) );
            }
            flag2 = true;
            break;
        }
        case API_EVENT_ID_SMS_SENT:                             //1
            Trace(2,"Send Message Success");
            UART_Write(UART1,"message sent\t",strlen("message sent\t"));
            break;

        case API_EVENT_ID_SMS_RECEIVED:    
                                 
            SendSmsUnicode();
            Trace(2,"received message");
            SMS_Encode_Type_t encodeType = pEvent->param1;
            uint32_t contentLength = pEvent->param2;
            uint8_t* header = pEvent->pParam1;
            uint8_t* content = pEvent->pParam2;
            if(!SMS_DeleteMessage(5,SMS_STATUS_ALL,SMS_STORAGE_SIM_CARD)){
                Trace(1,"delete sms fail");
                //UART_Write(UART1,"delete sms fail",strlen("delete sms fail"));
            }else{
                Trace(1,"delete sms success");
                //UART_Write(UART1,"delete sms success\t",strlen("delete sms success\t"));
            }
            
          Trace(2,"message header:%s",header);
            Trace(2,"message content length:%d",contentLength);
            
            
            UART_Write(UART1,"SMS received:",strlen("SMS received:"));
            //UART_Write(UART1,header,strlen(header));
            
            
            UART_Write(UART1,content,strlen(content));
            UART_Write(UART1,"\t",strlen("\t"));
            uint8_t* status= "status?";
            uint8_t* reset= "reset";
            
            //UART_Write(UART1,msg,strlen(msg));

           
            
            int x=strlen(content);
            int y=strlen(status);
            int z=strlen(reset);
            if(x == y && memcmp(content, status, x) == 0){
                
                uint8_t batt;
                uint16_t v = PM_Voltage(&batt);
                snprintf(buffer,400,"IMEI:%s,Battery:%d",imei,batt);
                UART_Write(UART1,buffer,strlen(buffer));
                strcpy(utf8Msg,buffer);
                //strcat(utf8Msg,percent);
                SendSMS();
                
            }else if(x == z && memcmp(content, reset, x) == 0){
                
                snprintf(buffer,400,"%s","Reset initiated\t");
                UART_Write(UART1,buffer,strlen(buffer));
                strcpy(utf8Msg,buffer);
                SendSMS();
                OS_Sleep(5000);
                PM_Restart();
            }
            
            if(encodeType == SMS_ENCODE_TYPE_ASCII)
            {
                Trace(2,"message content:%s",content);
                //UART_Write(UART1,content,contentLength);
                
            
            }
            else
            {
                uint8_t tmp[500];
                memset(tmp,0,500);
                for(int i=0;i<contentLength;i+=2)
                    sprintf(tmp+strlen(tmp),"\\u%02x%02x",content[i],content[i+1]);
                Trace(2,"message content(unicode):%s",tmp);//you can copy this string to http://tool.chinaz.com/tools/unicode.aspx and display as Chinese
                uint8_t* gbk = NULL;
                uint32_t gbkLen = 0;
                if(!SMS_Unicode2LocalLanguage(content,contentLength,CHARSET_CP936,&gbk,&gbkLen))
                    Trace(10,"convert unicode to GBK fail!");
                else
                {
                    memset(tmp,0,500);
                    for(int i=0;i<gbkLen;i+=2)
                        sprintf(tmp+strlen(tmp),"%02x%02x ",gbk[i],gbk[i+1]);
                    Trace(2,"message content(GBK):%s",tmp);//you can copy this string to http://m.3158bbs.com/tool-54.html# and display as Chinese
                    UART_Write(UART1,gbk,gbkLen);//use serial tool that support GBK decode if have Chinese, eg: https://github.com/Neutree/COMTool
                }
                OS_Free(gbk);
            }
            break;
        default:
            break;
    }
}


OS_Heap_Status_t heapStatus;

 
void decToHexa(int n) 
{    

    int i = 0; 
    while(n!=0) 
    {    
        int temp  = 0; 
        temp = n % 16; 
        if(temp < 10) 
        { 
            hexaDeciNum[i] = temp + 48; 
            i++; 
        } 
        else
        { 
            hexaDeciNum[i] = temp + 55; 
            i++; 
        } 
          
        n = n/16; 
    } 
      
     
    for(int j=i-1; j>=0; j--) 
        hexaDeciNumR[i-j-1]=hexaDeciNum[j];  
}



int Http_Post(const char* domain, int port,const char* path,uint8_t* body, uint16_t bodyLen, char* retBuffer, int bufferLen)
{
    
    uint8_t ip[16];
    bool hflag = false;
    uint16_t recvLen = 0;
    memset(ip,0,sizeof(ip));
    if(DNS_GetHostByName2(domain,ip) != 0)
    {
        return -1;
    }
    
    char* temp = OS_Malloc(2048);
    if(!temp)
    {
        return -1;
    }
    uint8_t percent;
    uint16_t v = PM_Voltage(&percent);

    RTC_Time_t time;
    bool retooo = TIME_GetRtcTime(&time);
    int dd=(time.year);
    char d[10];
    snprintf(d,10,"%d",dd);
    char yy[10];
    yy[0]=d[2];
    yy[1]=d[3];

    UART_Write(UART1,yy,strlen(yy));

    if(time.timeZone>0)
        
        snprintf(t,2048,"%s/%d/%d,%02d:%02d:%02d",yy,time.month,time.day,time.hour,time.minute,time.second);
    else
        snprintf(t,2048,"%02d:%02d:%02d",time.hour,time.minute,time.second); 
    
    snprintf(temp,2048,"%s;%s;%d;%s",imei,t,percent,path);
    char* pData = temp;
    int fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(fd < 0){

        OS_Free(temp);
        return -1;
    }

    struct sockaddr_in sockaddr;
    memset(&sockaddr,0,sizeof(sockaddr));
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_port = htons(port);
    inet_pton(AF_INET,servInetAddr,&sockaddr.sin_addr);

    int ret = connect(fd, (struct sockaddr*)&sockaddr, sizeof(struct sockaddr_in));
    if(ret < 0){

        close(fd);
        return -1;
    }
  
    ret = send(fd, pData, strlen(pData), 0);
    if(ret < 0){

        OS_Free(temp);
        close(fd);
        return -1;
    }
    ret = send(fd, body, bodyLen, 0);
    if(ret < 0){
        
        OS_Free(temp);
        close(fd);
        return -1;
    }

    struct fd_set fds;
    struct timeval timeout={12,0};
    FD_ZERO(&fds);
    FD_SET(fd,&fds);
    while(!hflag)
    {
        ret = select(fd+1,&fds,NULL,NULL,&timeout);

        switch(ret)
        {
            case -1:
                //Trace(2,"select error");
                hflag = true;
                break;
            case 0:
                Trace(2,"select timeout");
                hflag = true;
                break;
            default:
                if(FD_ISSET(fd,&fds))
                {
                    memset(retBuffer,0,bufferLen);
                    ret = recv(fd,retBuffer,bufferLen,0);
                    recvLen += ret;
                    if(ret < 0)
                    {
                        //Trace(2,"recv error");
                        hflag = true;
                        break;
                    }
                    else if(ret == 0)
                    {
                        //Trace(2,"ret == 0");
                        break;
                    }
                    else if(ret < 1352)
                    {
                        //GPS_DEBUG_I("recv len:%d,data:%s",recvLen,retBuffer);
                        close(fd);
                        OS_Free(temp);
                        return recvLen;
                    }
                }
                break;
        }
    }
    close(fd);
    OS_Free(temp);
    return -1;
}

void SendSmsUnicode()
{
    Trace(1,"sms start send unicode message");
    if(!SMS_SendMessage(TEST_PHONE_NUMBER,unicodeMsg,sizeof(unicodeMsg),SIM0))
    {
        Trace(1,"sms send message fail");
    }
}


void SecondTask(void *pData)
{
    char ip[16];

    
    int count=0;

    while(!flag)
    {
        //Trace(1,"wait for network register");
        OS_Sleep(3000);
    } 
    TIME_SetIsAutoUpdateRtcTime(true);
    SendSMS();

    while(1)
    {

        while(!flag)
        {
            //Trace(1,"wait for network register");
            OS_Sleep(3000);
        }  
        


        

        if(!Network_GetCellInfoRequst())
        {
           // Trace(1,"network get cell info fail");
        }
        else
        {
            flag2 = false;
        }
        while(!flag2)
        {
            //Trace(1,"wait for lbs result");
            OS_Sleep(2000);
        }
        //Trace(1,"times count:%d",++count);

        uint8_t status;
            Network_GetActiveStatus(&status);
            if(status)
            {
                GPIO_Set(UPLOAD_DATA_LED,GPIO_LEVEL_HIGH);
                //SendSMS(); 
                UART_Write(UART1,"uploading data\t",strlen("uploading data\t"));
                if(Http_Post(SERVER_IP,SERVER_PORT,final,NULL,0,final,sizeof(final)) <0 )
                    Trace(1,"send location to server fail");
                else
                {
                    Trace(1,"send location to server success");
                    //Trace(1,"response:%s",info);
                }
                GPIO_Set(UPLOAD_DATA_LED,GPIO_LEVEL_LOW);
            }
            else
            {
                Trace(1,"no internet");
            }
            
            
            PM_SleepMode(true);
         
            OS_Sleep(60000);
            OS_Sleep(101000);

            OS_Sleep(120000);
            OS_Sleep(120000);
            OS_Sleep(120000);
            OS_Sleep(120000); 

            
            PM_SleepMode(false);
            

        }    
               
    }


void LED_Blink(void* param)
{
    static int count = 0;
    if(++count == 5)
    {
        GPIO_Set(SYSTEM_STATUS_LED,GPIO_LEVEL_HIGH);
    }
    else if(count== 6)
    {
        GPIO_Set(SYSTEM_STATUS_LED,GPIO_LEVEL_LOW);
        count = 0;
    }
    OS_StartCallbackTimer(mainTaskHandle,1000,LED_Blink,NULL);
}


void MainTask(void *pData)
{
    API_Event_t* event=NULL;

    TIME_SetIsAutoUpdateRtcTime(true);
    if(!INFO_GetIMEI(imei))
        Assert(false,"NO IMEI");

    UART_Config_t config = {
        .baudRate = UART_BAUD_RATE_9600,
        .dataBits = UART_DATA_BITS_8,
        .stopBits = UART_STOP_BITS_1,
        .parity   = UART_PARITY_NONE,
        .rxCallback = NULL,
        .useEvent   = true
    };

    UART_Init(UART1,config);


    GPIO_config_t gpioLedBlue = {
        .mode         = GPIO_MODE_OUTPUT,
        .pin          = SYSTEM_STATUS_LED,
        .defaultLevel = GPIO_LEVEL_LOW
    };
    

    GPIO_config_t gpioLedUpload = {
        .mode         = GPIO_MODE_OUTPUT,
        .pin          = UPLOAD_DATA_LED,
        .defaultLevel = GPIO_LEVEL_LOW
    }; 

    PM_PowerEnable(POWER_TYPE_VPAD,false);     //was initially false

    PM_PowerEnable(POWER_TYPE_MMC, false); // Power off from GPIO8  ~ GPIO13
    PM_PowerEnable(POWER_TYPE_LCD, false); // Power off from GPIO14 ~ GPIO18
    PM_PowerEnable(POWER_TYPE_CAM, false); // Power off from GPIO19 ~ GPIO24


    //GPIO_Init(gpioLedBlue);
    GPIO_Init(gpioLedUpload);
    SMSInit();

   // OS_StartCallbackTimer(mainTaskHandle,1000,LED_Blink,NULL);
   

    secondTaskHandle = OS_CreateTask(SecondTask,
        NULL, NULL, SECOND_TASK_STACK_SIZE, SECOND_TASK_PRIORITY, 0, 0, SECOND_TASK_NAME);

    while(1)
    {
        if(OS_WaitEvent(mainTaskHandle, (void**)&event, OS_TIME_OUT_WAIT_FOREVER))
        {
            EventDispatch(event);
            OS_Free(event->pParam1);
            OS_Free(event->pParam2);
            OS_Free(event);
        }
    }
}

void lbs_Main()
{
    mainTaskHandle = OS_CreateTask(MainTask ,
        NULL, NULL, MAIN_TASK_STACK_SIZE, MAIN_TASK_PRIORITY, 0, 0, MAIN_TASK_NAME);
    OS_SetUserMainHandle(&mainTaskHandle);
}

void SMSInit()
{
     UART_Write(UART1,"sms initialised\t",strlen("sms initialised\t"));
    if(!SMS_SetFormat(SMS_FORMAT_TEXT,SIM0))
    {
        Trace(1,"sms set format error");
        return;
    }
    SMS_Parameter_t smsParam = {
        .fo = 17 ,
        .vp = 167,
        .pid= 0  ,
        .dcs= 8  ,
    };
    if(!SMS_SetParameter(&smsParam,SIM0))
    {
        Trace(1,"sms set parameter error");
        return;
    }
    if(!SMS_SetNewMessageStorage(SMS_STORAGE_SIM_CARD))
    {
        Trace(1,"sms set message storage fail");
        return;
    }
}
void SendUtf8()
{
    uint8_t* unicode = NULL;
    uint32_t unicodeLen;

    Trace(1,"sms start send UTF-8 message");

    if(!SMS_LocalLanguage2Unicode(utf8Msg,strlen(utf8Msg),CHARSET_UTF_8,&unicode,&unicodeLen))
    {
        Trace(1,"local to unicode fail!");
        return;
    }
    if(!SMS_SendMessage(TEST_PHONE_NUMBER,unicode,unicodeLen,SIM0))
    {
        Trace(1,"sms send message fail");
        UART_Write(UART1,"sms failed\n",strlen("sms failed\n"));
    }
    OS_Free(unicode);
}
void SendSMS()
{
    SendUtf8();
}
void ServerCenterTest()
{
    uint8_t addr[32];
    uint8_t temp;
    SMS_Server_Center_Info_t sca;
    sca.addr = addr;
    SMS_GetServerCenterInfo(&sca);
    Trace(1,"server center address:%s,type:%d",sca.addr,sca.addrType);
    temp = sca.addr[strlen(sca.addr)-1];
    sca.addr[strlen(sca.addr)-1] = '0';
    if(!SMS_SetServerCenterInfo(&sca))
        Trace(1,"SMS_SetServerCenterInfo fail");
    else
        Trace(1,"SMS_SetServerCenterInfo success");
    SMS_GetServerCenterInfo(&sca);
    Trace(1,"server center address:%s,type:%d",sca.addr,sca.addrType);
    sca.addr[strlen(sca.addr)-1] = temp;
    if(!SMS_SetServerCenterInfo(&sca))
        Trace(1,"SMS_SetServerCenterInfo fail");
    else
        Trace(1,"SMS_SetServerCenterInfo success");
}
